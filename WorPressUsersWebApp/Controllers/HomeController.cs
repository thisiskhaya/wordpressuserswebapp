﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WorPressUsersWebApp.Models;

namespace WorPressUsersWebApp.Controllers
{
    public class HomeController : Controller
    {
        public async Task<ActionResult> Index()
        {
            await AddLearnersAsync();
            return View();
            
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }



        public async Task AddLearnersAsync()
        {
            ArrayList learners = this.getLearners();
            foreach(Learner learner in learners)
            {
                await TestAsyncPost(learner.UserName, learner.Email, learner.Password);
            }
        }

        private static async Task TestAsyncPost(string username, string email, string password){

            Trace.WriteLine("Adding learner");
            var values = new Dictionary<string, string>();
            values.Add("username", username);
            values.Add("email", email);
            values.Add("password", password);
            values.Add("roles", "subscriber");
            var content = new FormUrlEncodedContent(values);
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue(
                        "Basic",
                        Convert.ToBase64String(
                            System.Text.ASCIIEncoding.ASCII.GetBytes(
                                string.Format("{0}:{1}", "khaya", "password"))));
                try
                {
                    var httpResponseMessage = await client.PostAsync("http://wordpress-learning/wp-json/wp/v2/users", content);
                    if (httpResponseMessage.StatusCode == HttpStatusCode.OK)
                    {

                        var response = await httpResponseMessage.Content.ReadAsStringAsync();
                        Trace.WriteLine("RESPONSE" + response + "Here");
                    }
                }
                catch (Exception ex)
                {
                    Trace.WriteLine("Exception" + ex.ToString());
                }
            }
            Trace.WriteLine("Finished adding learner");
        }

        public ArrayList getLearners()
        {
            ArrayList learners = new ArrayList();
            learners.Add(new Learner { UserName = "user22", Email = "example@ido.ie", Password = "xxxx", Role = "Subscriber" });
            learners.Add(new Learner { UserName = "user23", Email = "example@idso.ie", Password = "xxxx", Role = "Subscriber" });
            learners.Add(new Learner { UserName = "user24", Email = "example@idsoo.ie", Password = "xxxx", Role = "Subscriber" });
            learners.Add(new Learner { UserName = "user25", Email = "example@pdsoo.ie", Password = "xxxx", Role = "Subscriber" });
            return learners;
        }


    }
}